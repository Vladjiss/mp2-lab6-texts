#include "..\include\TText.h"

int main()
{
    TTextLink::InitMemSystem(14);
    TText text;
	
    text.Read("input.txt");
    text.GoFirstLink();
    text.GoDownLink();
    text.DelDownSection();

    TTextLink::MemCleaner(text);

    text.Print();
	getchar();
    text.Write("output.txt");
}