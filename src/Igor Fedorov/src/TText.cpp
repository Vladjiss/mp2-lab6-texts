#include "..\include\TText.h"

const int buf = 80;
static int TextLevel;

//�����������
TText::TText(PTTextLink pl) {
	if (pl == nullptr) 
		pl = new TTextLink();
	pFirst = pl;
}

PTText TText::getCopy()
{
	PTTextLink pl1, pl2 = nullptr, pl = pFirst, cpl = nullptr;

	if (pFirst != nullptr) {
		while (!St.empty()) St.pop();
		while (true)
		{
			if (pl != nullptr) {
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == nullptr) {
					pl2 = new TTextLink("Copy", pl1, cpl);
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = nullptr;
				}
				else {
					strcpy_s(pl1->Str, pl1->pNext->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
		SetRetCode(TextOK);
	}
	else {
		SetRetCode(TextError);
	}
	return new TText(cpl);
}

//���������
int TText::GoFirstLink(void) {
	while (!Path.empty()) 
		Path.pop();
	pCurrent = pFirst;
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) {
	SetRetCode(TextNoDown);
	if (pCurrent != nullptr)
		if (pCurrent->pDown != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) {
	SetRetCode(TextNoNext);
	if (pCurrent != nullptr)
		if (pCurrent->pNext != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

//������
string TText::GetLine(void) {
	if (pCurrent == nullptr)
		return string("");
	else
		return string(pCurrent->Str);
}

void TText::SetLine(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		strcpy_s(pCurrent->Str, s.c_str());
}

//�����������
void TText::InsDownLine(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
        SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
		SetRetCode(TextOK);
	}
}
void TText::InsNextSection(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(TextNoDown);
    else if (pCurrent->pDown->IsAtom())
        pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(TextNoDown);
    else
        pCurrent->pDown = nullptr;
}

void TText::DelNextLine(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(TextNoNext);
    else if (pCurrent->pNext->IsAtom())
        pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(TextNoNext);
    else
        pCurrent->pNext = pCurrent->pNext->pNext;
}

//��������
int TText::Reset(void) {
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != nullptr) {
		St.push(pCurrent);
		if (pCurrent->pNext != nullptr)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != nullptr)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

bool TText::IsTextEnded(void) const {
	return !St.size();
}

int TText::GoNext(void) {
	if (!IsTextEnded()) {
		pCurrent = St.top(); St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != nullptr)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != nullptr)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

//����������� ������
PTTextLink TText::GetFirstAtom(PTTextLink pl) {
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

//������
void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != nullptr) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
        cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile)
{
    if (ptl != nullptr) {
        for (int i = 0; i < TextLevel; i++)
            TxtFile << " ";
        TxtFile << ptl->Str << endl;
        TextLevel++; PrintTextFile(ptl->GetDown(), TxtFile);
        TextLevel--; PrintTextFile(ptl->GetNext(), TxtFile);
    }
}

//������
void TText::Read(char* pFileName) {
	ifstream TxtFile(pFileName);
	TextLevel = 0;
	if (&TxtFile != nullptr)
		pFirst = ReadText(TxtFile);
}

PTTextLink TText::ReadText(ifstream& TxtFile)
{
    string buf;
    PTTextLink ptl = new TTextLink();
    PTTextLink tmp = ptl;
    while (!TxtFile.eof())
    {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            ptl->pDown = ReadText(TxtFile);
        else
        {
            ptl->pNext = new TTextLink(buf.c_str());
            ptl = ptl->pNext;
        }
    }
    ptl = tmp;
    if (tmp->pDown == nullptr)
    {
        tmp = tmp->pNext;
        delete ptl;
    }
    return tmp;
}

void TText::Write(char * pFileName)
{
    TextLevel = 0;
    ofstream TextFile(pFileName);
    PrintTextFile(pFirst, TextFile);
}
